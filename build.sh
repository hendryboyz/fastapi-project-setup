#!/bin/bash
[ $(docker image ls fastapi:latest | wc -l)  = 2 ]  && docker rmi -f fastapi:latest
docker build -t fastapi:latest -f dockerfile .