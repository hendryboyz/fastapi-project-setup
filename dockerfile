ARG PORT=80
FROM python:3.9-alpine

WORKDIR /app
COPY ./requirements.txt /app/requirements.txt

RUN apk add --no-cache --virtual=.build-dependencies \
    alpine-sdk \
  && python -m pip install --upgrade pip \
  && pip install --no-cache-dir --upgrade -r requirements.txt \
  && apk del .build-dependencies

EXPOSE ${PORT}

COPY ./api/ /app/api/
CMD uvicorn api.main:app --host 0.0.0.0 --port $PORT