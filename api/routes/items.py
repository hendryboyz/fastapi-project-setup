from typing import Optional
from fastapi import APIRouter, Depends
from api.core.entities.items import Item
from ..dependencies import get_token_header

router = APIRouter(
    prefix='/items',
    tags=['items'],
    dependencies=[Depends(get_token_header)]
)

serial_number = 0
items_db = []

@router.post('/', status_code=201)
async def create_item(item: Item):
    global serial_number
    serial_number += 1
    item.id = serial_number
    items_db.append(item)
    item_dict = item.dict()
    if item.tax:
        price_with_tax = item.price + item.tax
        item_dict.update({ 'price_with_tax': price_with_tax })
    return item



@router.get('/', status_code=200)
async def list_item(skip: int = 0, limit: int = 10):
    return items_db[skip : skip + limit]

@router.get('/{item_id}', status_code=200)
async def get_item(item_id: int, needy: str):
    item = { 'item_id': item_id, 'needy': needy }
    target = next(item for item in items_db if item.id == item_id)
    return target if target else None

@router.put(
    '/{item_id}',
    status_code=200,
)
async def update_item(item_id: str, item: Item, q: Optional[str] = None):
    return { 'item_id': item_id, **item.dict() }
