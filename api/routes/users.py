from typing import Optional
from fastapi import APIRouter

router = APIRouter()

@router.get(
    '/{user_id}/items/{item_id}',
    status_code=200,
    tags=['items']
)
async def get_user_item(
    user_id: int, item_id: str, q: Optional[str] = None, short: bool = False):
    item = { 'item_id': item_id, 'owner_id': user_id }
    if q:
        item.update({ 'q': q })
    if not short:
        item.update({
            'description': 'This is an amazing item that has a long description'
        })
    return item