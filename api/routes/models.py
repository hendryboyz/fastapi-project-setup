from enum import Enum
from fastapi import APIRouter

router = APIRouter(
   prefix='/models',
   tags=['models'] 
)

class ModelName(str, Enum):
    alexnet = "alexnet"
    resnet = "resnet"
    lenet = "lenet"

@router.get('/{model_name}')
async def get_model(model_name: ModelName):
    message = get_message(model_name=model_name)
    return { 'model_name': model_name, 'message': message }

def get_message(model_name: ModelName):
    if model_name == ModelName.alexnet:
        return 'Deep Learning FTW!'
    elif model_name == ModelName.resnet:
        return 'LeCNN all the images'
    else:
        return 'Have some residuals'