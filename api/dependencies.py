from fastapi import Header, HTTPException

async def get_token_header(x_token: str = Header(...)):
    if not x_token:
        raise HTTPException(status_code=401, detail='Require header: x_token')