from fastapi import FastAPI

from .routes import items, models, users

app = FastAPI()

@app.get('/healthcheck')
async def healthcheck():
    return { 'status': 'ok' }

app.include_router(router=items.router)
app.include_router(router=models.router)
app.include_router(
    router=users.router,
    prefix='/users',
    tags=['users']
)